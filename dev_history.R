golem::create_golem(path = "../shinygolem")

usethis::use_readme_rmd()
rmarkdown::render("README.Rmd")

# build ignore ----
usethis::use_build_ignore("dev_history.R")
