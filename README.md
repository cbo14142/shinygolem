
<!-- README.md is generated from README.Rmd. Please edit that file -->

## shinygolem

<!-- badges: start -->

<!-- badges: end -->

The goal of shinygolem is to test out `golem` and the handmade
`shinylib`.

## About `golem`

Golem est un package créé par `ThinkR`, une entreprise française de
conseil. Il existe de nombreux tutorial très bien écrit sur leur site
web et leur blog :

  - <https://rtask.thinkr.fr/our-shiny-template-to-design-a-prod-ready-app/>
    (plus ancien mais général)
  - <https://rtask.thinkr.fr/getting-started-with-golem/>
  - <https://thinkr-open.github.io/golem/>

> A l’initialisation du projet, Golem créé les dossiers et fichiers
> nécessaires au developpement dans le dossier dev.

## Objectifs

  - Découvrir golem et sa méthode de développement des apps
  - Tester `shinylib`
  - Simuler un développement en “prod”
      - avec Docker
      - la CI/CD de Gitlab

### 1\) Créer une app avec l’aide de Golem et shinylib

Cette app doit passer un `check` au niveau 0 error, 0 warning, 0 note.

### 2\) Ajouter un Dockerfile

On pourra s’aider (si besoin) de la fonction :

``` r
golem::add_dockerfile()
```

> Cette configuration est valide avec SAAGIE manager. Pour créer une
> application en v2 il faut passer par une configuration de type shiny
> server

### 3\) Configuration CI

Gitlab configure le pipline d’integration continue (CI) avec un fichier
`gitlab-ci.yml`.

  - 3.1 Créer ce fichier avec :

<!-- end list -->

``` r
stages:
  - build-image

build-testing-image:
  stage: build-image
  image: docker:19.03.1
  services:
    - docker:19.03.1-dind
  variables:
    DOCKER_HOST: tcp://docker:2375/
  before_script:
    - echo $CI_REGISTRY_PASSWORD | docker login -u $CI_REGISTRY_USER --password-stdin $CI_REGISTRY
  script:
    - docker pull $CI_REGISTRY_IMAGE:latest || true
    - docker build --cache-from $CI_REGISTRY_IMAGE:latest --tag $CI_REGISTRY_IMAGE:$CI_COMMIT_SHA --tag $CI_REGISTRY_IMAGE:latest .
    - docker build --tag $CI_REGISTRY_IMAGE:$CI_COMMIT_SHA .
    - docker push $CI_REGISTRY_IMAGE:$CI_COMMIT_SHA
```

Références :

  - <https://gitlab.com/help/user/packages/container_registry/index>
  - <https://gitlab.com/ecohealthalliance/drake-gitlab-docker-example>
  - <https://gitlab.com/snippets/1761292>

> NB : les procédures pour se logger au docker registry peuvent varier
> pour un répertoire Gitlab d’entreprise à accès restreint

  - Affecter un server pour l’exécution du pipeline. Trouver un `runner`
    publique dans Settings \> CI/CD \> Runners et ajouter son tag à la
    suite du fichier de configuration `.gitlab-ci.yml` :

<!-- end list -->

``` r
  tag:
    mytag
```

  - 3.3 Faire un push et vérifier la bonne exécution du pipeline

  - 3.3 Customisation
    
      - 3.3.1 Ajouter une première étape “check-package” (stage) avec un
        `check()` du package dans le pipeline (il faudra installer toute
        les dépendances R au préalable).
    
      - 3.3.2 Effectuer l’étape “check-package” uniquement sur la
        branche `dev`

### 4\) Configuration CD

Ajouter une étape “deploy-image” dans le fichier de configuration
exécuté si il n’y a pas eu de problème à l’étape précédente.

``` r
deploy-image-step:
  stage: deploy-image
  image: docker:19.03.1
  services:
    - docker:19.03.1-dind
  variables:
    DOCKER_HOST: tcp://docker:2375/
    DOCKER_NAME: ''
    DOCKER_TAG: 'latest'
    DEV_JOB_ID: ''
    PROD_JOB_ID: ''
    DEV_URL: ''
    PROD_URL: ''
    PLATFORM_URL: ''
    ID: $PROJECT_ID
    PW: $PROJECT_PW
  before_script:
    - echo $CI_REGISTRY_PASSWORD | docker login -u $CI_REGISTRY_USER --password-stdin $CI_REGISTRY
  script:
    - echo "Manual deployment to integration environment..."
    - "curl \"$PLATFORM_URL$INT_JOB_ID\" -X POST -v -u $ID:$PW"
    - "curl \"$PLATFORM_URL$INT_JOB_ID/stop\" -X POST -v -u $ID:$PW; sleep 15; curl \"$PLATFORM_URL$INT_JOB_ID/run\" -X POST -v -u \"$ID:$PW\""
  when: on_success
  tags:
   - gitlab-org
```

## Discussion

Gitlab propose d’autre manière de faire de la CI/CD, par exemple avec
[kaniko](https://docs.gitlab.com/ee/ci/docker/using_kaniko.html). Il est
également possible d’utiliser le [plugin gradle de
SAAGIE](https://github.com/saagie/gradle-saagie-dataops-plugin/wiki/projectsUpgradeJob)
pour spécifier la version de l’image docker déployée (les messages
d’erreurs peuvent être difficile à debugger).

Avec ce projet, nous avons simulé le cycle de développement d’une
application ou d’un package. On pourrait compléter ce sujet par exemple
en y ajoutant des tests shiny (avec la package `shinytest`) ou une
documentation pour le nouvel utilisateur sous forme de markdown pour
guider la navigation dans l’app.

Autres références utiles :

Packages R

  - <http://r-pkgs.had.co.nz/>
  - <https://kbroman.org/pkg_primer/pages/build.html>
  - <https://thinkr.fr/creer-package-r-quelques-minutes/>

Gitlab

  - <https://docs.gitlab.com/ee/ci/variables/predefined_variables.html>
  - <https://docs.gitlab.com/ee/user/project/deploy_tokens/>
  - <https://docs.gitlab.com/ee/user/profile/personal_access_tokens.html>
